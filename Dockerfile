FROM nginx

COPY etc/nginx/ /etc/nginx/conf.d/
COPY ./src /var/www
