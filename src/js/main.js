// const BASE_API_URL = 'http://localhost:4000'; //local
const BASE_API_URL = 'http://api.pjtuxe.com'; //prod

var actualRound = 1;
var maxRound = 20;
var isPlay = true;
var all_coordinates = [];
var actualDataSet = [];
var stepInterval = [];

jQuery(function () {
    console.log('App started...');

    $(".timeline-panel__time").html(
        getCurrentDate() + ' ' + getCurrentTime()
    );

    mapboxgl.accessToken = 'pk.eyJ1IjoiaGl0ZXJ0YW1hcyIsImEiOiJjam5na3B2eGUwMHdxM3FzODE3N3Q4b202In0.YKQf0eYqkvC9anlkDO18CA';

    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/hitertamas/ckfss06n440uq19k2mhsxfess',
        center: [-118.2, 34],
        zoom: 7.5,
        minZoom: 7,
        maxZoom: 10,
        maxBounds: [
            [-124, 31],
            [-116, 38],
        ],
        position: 'fixed',
    });

    map.getCanvas().style.cursor = 'initial';

    map.setPitch(60);

    map.on('load', function () {
        disableLoader();
        showHelpText();
        generateCities();

        initDefaultSources();

        map.on("click", function(e) {
            let lng = e.lngLat['lng'];
            let lat = e.lngLat['lat'];

            window.HackathonConfigurator.setConfiguration('latitude', lat);
            window.HackathonConfigurator.setConfiguration('longitude', lng);

            generateMarker(lng, lat);

            map.flyTo({
                center: [lng, lat],
                speed: 0.5,
                zoom: 8,
            });
        });

        map.on('mousemove', function(e) {
            $(".long-lat-container__long-element").html(e.lngLat['lng'].toFixed(6));
            $(".long-lat-container__lat-element").html(e.lngLat['lat'].toFixed(6));
        });

        var quakeID = null;

        function mouseMoveFn(e) {
            map.getCanvas().style.cursor = 'pointer';
            // Check whether features exist
            if (e.features.length > 0) {
                // var temperature = e.features[0].properties.temp - 273;  // Celsius
                var temperature = (e.features[0].properties.temp * 9/5) - 459.67;
                window.HackathonConfigurator.setConfiguration('temperature', temperature.toFixed(2));
                window.HackathonConfigurator.setConfiguration('wind_speed', e.features[0].properties.wind_speed);
                window.HackathonConfigurator.setConfiguration('wind_deg', e.features[0].properties.wind_deg);
                window.HackathonConfigurator.setConfiguration('humidity', e.features[0].properties.humidity);
                window.HackathonConfigurator.setConfiguration('confidence', e.features[0].properties.confidence);

                if (quakeID) {
                    map.removeFeatureState({
                        source: "data-fire-yellow",
                        id: quakeID
                    });
                }

                quakeID = e.features[0].id;

                map.setFeatureState({
                    source: 'data-fire-yellow',
                    id: quakeID,
                }, {
                    hover: true
                });

            }
        }

        function mouseLeaveFn() {
            if (quakeID) {
                map.setFeatureState({
                    source: 'data-fire-yellow',
                    id: quakeID
                }, {
                    hover: false
                });
            }
            quakeID = null;

            map.getCanvas().style.cursor = 'initial';
        }

        map.on('mousemove', 'data-fire-yellow-layer', mouseMoveFn);
        map.on('mousemove', 'data-fire-orange-layer', mouseMoveFn);
        map.on('mousemove', 'data-fire-red-layer', mouseMoveFn);

        map.on("mouseleave", "data-fire-yellow-layer", mouseLeaveFn);
        map.on("mouseleave", "data-fire-orange-layer", mouseLeaveFn);
        map.on("mouseleave", "data-fire-red-layer", mouseLeaveFn);
    });

    $("#button--apply-configuration").click(function () {
        let lng = $("#longitude").val();
        let lat = $("#latitude").val();

        stopButtonPulse();
        hideStartButton();

        if (lng.length > 0 && lat.length > 0) {
            /* Map clear */
            removeMapSourcesSources();
            initDefaultSources();
            clearInterval(stepInterval);

            isPlay = true;
            generateMarker(lng, lat);
            fetchCoordinates(lat, lng);
            addButtonLoader();
        }
    });

    /**
     *
     */
    function addButtonLoader() {
        $('#button--apply-configuration').html(' <i id="apply-configuration-spinner"></i>');
        $("#apply-configuration-spinner").addClass('fa fa-spinner fa-spin');
    }

    function removeButtonLoader() {
        $("#apply-configuration-spinner").removeClass('glyphicon glyphicon-refresh spinning');
        $('#button--apply-configuration').html('PREDICT FIRE SPREAD');
    }

    /**
     *
     */
    function initDefaultSources() {
        map.addSource('data-fire-red', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'geometry':
                    {
                        'type': 'Point',
                        'coordinates': [
                            0,0
                        ],
                        'properties': {
                            'title': 'Fire'
                        }
                    },
            },
            'generateId': true
        });

        map.addSource('data-fire-orange', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'geometry':
                    {
                        'type': 'Point',
                        'coordinates': [
                            0,0
                        ],
                        'properties': {
                            'title': 'Fire'
                        }
                    },
            },
            'generateId': true
        });

        map.addSource('data-fire-yellow', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'geometry':
                    {
                        'type': 'Point',
                        'coordinates': [
                            0,0
                        ],
                        'properties': {
                            'title': 'Fire'
                        }
                    },
            },
            'generateId': true
        });

        map.addLayer({
            'id': 'data-fire-red-layer',
            'type': 'circle',
            'source': 'data-fire-red',
            'paint': {
                'circle-radius': [
                    'case',
                    ['boolean',
                        ['feature-state', 'hover'],
                        true
                    ],
                    [
                        'interpolate', ['linear'],
                        ['get', 'mag'],
                        1, 8,
                        1.5, 10,
                        2, 12,
                        2.5, 14,
                        3, 16,
                        3.5, 18,
                        4.5, 20,
                        6.5, 22,
                        8.5, 24,
                        10.5, 26
                    ],
                    5
                ],
                'circle-stroke-color': '#ff0000',
                'circle-stroke-width': 16,
                'circle-color': [
                    'case',
                    ['boolean',
                        ['feature-state', 'hover'],
                        false
                    ],
                    [
                        'interpolate', ['linear'],
                        ['get', 'mag'],
                        1, '#fff7ec',
                    ],
                    '#ff0000'
                ]
            }
        });

        map.addLayer({
            'id': 'data-fire-orange-layer',
            'type': 'circle',
            'source': 'data-fire-orange',
            'paint': {
                'circle-radius': [
                    'case',
                    ['boolean',
                        ['feature-state', 'hover'],
                        true
                    ],
                    [
                        'interpolate', ['linear'],
                        ['get', 'mag'],
                        1, 8,
                        1.5, 10,
                        2, 12,
                        2.5, 14,
                        3, 16,
                        3.5, 18,
                        4.5, 20,
                        6.5, 22,
                        8.5, 24,
                        10.5, 26
                    ],
                    5
                ],
                'circle-stroke-color': '#f86f1f',
                'circle-stroke-width': 12,
                'circle-color': [
                    'case',
                    ['boolean',
                        ['feature-state', 'hover'],
                        false
                    ],
                    [
                        'interpolate', ['linear'],
                        ['get', 'mag'],
                        1, '#fff7ec',
                    ],
                    '#f86f1f'
                ]
            }
        });

        map.addLayer({
            'id': 'data-fire-yellow-layer',
            'type': 'circle',
            'source': 'data-fire-yellow',
            'paint': {
                'circle-radius': [
                    'case',
                    ['boolean',
                        ['feature-state', 'hover'],
                        true
                    ],
                    [
                        'interpolate', ['linear'],
                        ['get', 'mag'],
                        1, 8,
                        1.5, 10,
                        2, 12,
                        2.5, 14,
                        3, 16,
                        3.5, 18,
                        4.5, 20,
                        6.5, 22,
                        8.5, 24,
                        10.5, 26
                    ],
                    5
                ],
                'circle-stroke-color': '#ffe700',
                'circle-stroke-width': 8,
                'circle-color': [
                    'case',
                    ['boolean',
                        ['feature-state', 'hover'],
                        false
                    ],
                    [
                        'interpolate', ['linear'],
                        ['get', 'mag'],
                        1, '#fff7ec',
                    ],
                    '#ffe700'
                ]
            }
        });
    }

    /**
     *
     */
    function removeMapSourcesSources() {
        isPlay = false;
        map.removeLayer('data-fire-red-layer');
        map.removeLayer('data-fire-orange-layer');
        map.removeLayer('data-fire-yellow-layer');
        map.removeSource('data-fire-red');
        map.removeSource('data-fire-orange');
        map.removeSource('data-fire-yellow');
    }

    /**
     * @param lng
     * @param lat
     */
    function generateMarker(lng, lat) {
        let marker = document.querySelector(".marker");

        if (marker) {
            marker.remove();
        } else {
            startButtonPulse();
        }

        hideHelpText();

        var markerElement = document.createElement('div');
        markerElement.className = 'marker';


        new mapboxgl.Marker(markerElement)
            .setLngLat(
                [
                    lng,
                    lat
                ]
            )
            .addTo(map);
    }

    /**
     *
     * @param lng
     * @param lat
     */
    function fetchCoordinates(lng, lat) {
        var queryString = Object
            .keys(window.HackathonConfigurator.configuration)
            .map(key => key + '=' + window.HackathonConfigurator.configuration[key])
            .join('&');

        $.get(getApiUrl("/prediction?" + queryString), function(data) {
            all_coordinates = data.data;
            automaticMapSourceSetting(data.data);
            return data;
        }).fail(function(e) {
            console.log('The request is not successfully');
        })
    }

    /**
     *
     */
    function preparePointFeatures(value) {
        value.config.confidence = value.confidence;
        return {
            'type': 'Feature',
            'geometry': {
                'type': 'Point',
                'coordinates': [
                    value.coords[0],
                    value.coords[1]
                ],
            },
            'properties': value.config,
        };
    }

    /**
     * @param data
     */
    function setMapSource(data) {
        removeButtonLoader();

        $(".timeline-panel__time").html(
            data.date.replace(/^(\d{4})-(\d{2})-(\d{2})$/, '$3/$2') + ' ' + data.time
        );

        var yellowFeatures = [];
        var orangeFeatures = [];
        var redFeatures = [];
        var lastConfig = [];

        $.each(data.alerts, function(key, value) {
            switch (value.color) {
                case "yellow":
                    yellowFeatures.push(preparePointFeatures(value))
                    lastConfig = value;
                    break;
                case "orange":
                    orangeFeatures.push(preparePointFeatures(value))
                    lastConfig = value;
                    break;
                case "red":
                    redFeatures.push(preparePointFeatures(value))
                    lastConfig = value;
                    break;
            }
        });

        if (lastConfig['config']) {
            var temperature = (lastConfig['config']['temp'] * 9/5) - 459.67;

            window.HackathonConfigurator.setConfiguration('wind_speed', lastConfig['config']['wind_speed'] ? lastConfig['config']['wind_speed'] : window.HackathonConfigurator.getConfiguration('wind_speed'));
            window.HackathonConfigurator.setConfiguration('wind_deg', lastConfig['config']['wind_deg'] ? lastConfig['config']['wind_deg'] : window.HackathonConfigurator.getConfiguration('wind_deg'));
            window.HackathonConfigurator.setConfiguration('humidity', lastConfig['config']['humidity'] ? lastConfig['config']['humidity'] : window.HackathonConfigurator.getConfiguration('humidity'));
            window.HackathonConfigurator.setConfiguration('elevation', lastConfig['config']['elevation'] ? lastConfig['config']['elevation'] : window.HackathonConfigurator.getConfiguration('elevation'));
            window.HackathonConfigurator.setConfiguration('temperature', lastConfig['config']['temp'] ? temperature.toFixed(0) : window.HackathonConfigurator.getConfiguration('temperature'));
        }

        map.getSource('data-fire-yellow').setData(
            {
                'type': 'FeatureCollection',
                'features': yellowFeatures,
            }
        );

        map.getSource('data-fire-orange').setData(
            {
                'type': 'FeatureCollection',
                'features': orangeFeatures,
            }
        )

        map.getSource('data-fire-red').setData(
            {
                'type': 'FeatureCollection',
                'features': redFeatures,
            }
        )
    }

    /**
     * @param data
     * @param x
     */
    function getCoordinateElement(data, x) {
        if (x > data.length) {
            return data[0];
        }

        return data[x];
    }

    /**
     *
     */
    function automaticMapSourceSetting(data) {
        if (maxRound > data.length) {
            maxRound = data.length;
        }

        removeTimelineDisabledFromButtons();

        stepInterval = setInterval(function () {
            if (isPlay) {
                actualRound++;

                if (actualRound === maxRound) {
                    actualRound = 0;
                }

                actualDataSet = data[actualRound];
                setMapSource(getCoordinateElement(data, actualRound));
            } else {
                clearInterval(stepInterval);
            }
        }, 2000);
    }

    /**
     *
     */
    function generateCities() {
        $.get(getApiUrl('/cities'), function (data) {
            var segmentedCities = segmentCitiesByPopulation(data);
            var large_features = prepareFeatures(segmentedCities.larges);
            var medium_features = prepareFeatures(segmentedCities.mediums);
            var small_features = prepareFeatures(segmentedCities.smalls);

            map.addSource('large_cities', {
                'type': 'geojson',
                'data': {
                    'type': 'FeatureCollection',
                    'features': large_features
                }
            });

            map.addLayer({
                'id': 'large_cities',
                'type': 'symbol',
                'source': 'large_cities',
                'layout': {
                    'text-field': [
                        'format',
                        ['upcase', ['get', 'title']],
                        { 'font-scale': 0.8 },
                        '\n',
                        {},
                        ['downcase', ['get', 'subtitle']],
                        { 'font-scale': 0.6 }
                    ],
                    'text-font': [
                        'Open Sans Semibold',
                        'Arial Unicode MS Bold'
                    ],
                    'text-offset': [0, 1.25],
                    'text-anchor': 'top',
                },
                'paint': {
                    "text-color": "#ffffff"
                },
            });

            map.addSource('medium_cities', {
                'type': 'geojson',
                'data': {
                    'type': 'FeatureCollection',
                    'features': medium_features
                }
            });

            map.addLayer({
                'id': 'medium_cities',
                'type': 'symbol',
                'source': 'medium_cities',
                'layout': {
                    'text-field': [
                        'format',
                        ['upcase', ['get', 'title']],
                        { 'font-scale': 0.8 },
                        '\n',
                        {},
                        ['downcase', ['get', 'subtitle']],
                        { 'font-scale': 0.6 }
                    ],
                    'text-font': [
                        'Open Sans Semibold',
                        'Arial Unicode MS Bold'
                    ],
                    'text-offset': [0, 1.25],
                    'text-anchor': 'top',
                },
                'paint': {
                    "text-color": "#ffffff"
                },
                'minzoom': 9,
            });

            map.addSource('small_cities', {
                'type': 'geojson',
                'data': {
                    'type': 'FeatureCollection',
                    'features': small_features
                }
            });

            map.addLayer({
                'id': 'small_cities',
                'type': 'symbol',
                'source': 'small_cities',
                'layout': {
                    'text-field': [
                        'format',
                        ['upcase', ['get', 'title']],
                        { 'font-scale': 0.8 },
                        '\n',
                        {},
                        ['downcase', ['get', 'subtitle']],
                        { 'font-scale': 0.6 }
                    ],
                    'text-font': [
                        'Open Sans Semibold',
                        'Arial Unicode MS Bold'
                    ],
                    'text-offset': [0, 1.25],
                    'text-anchor': 'top',
                },
                'paint': {
                    "text-color": "#ffffff"
                },
                'minzoom': 10,
            });

        }).fail(function() {
            console.log('The request for cities is not successfully');
        })
    }

    /**
     *
     */
    function segmentCitiesByPopulation(data) {
        var larges = [];
        var mediums = [];
        var smalls = [];

        data.forEach(function (city) {
            var population = parseInt(city.population);

            switch (true) {
                case population > 500000:
                    larges.push(city);
                    break;
                case population > 100000:
                    mediums.push(city);
                    break;
                default:
                    smalls.push(city);
            }
        });

        return {
            'smalls': smalls,
            'mediums': mediums,
            'larges': larges
        };
    }

    /**
     *
     */
    function prepareFeatures(cities) {
        var features = [];

        cities.forEach(function (city) {
            var feature = {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                        city.lng,
                        city.lat
                    ]
                },
                'properties': {
                    'title': city.city,
                    'subtitle': city.population.toString(),
                }
            };
            features.push(feature);
        });

        return features;
    }

    $(".timeline__start-button").click(function () {
        timelineStart();
    });

    $(".timeline__stop-button").click(function () {
        timelineStop();
    });

    $(".timeline__next-button").click(function () {
        getNextRound();
    });

    $(".timeline__prev-button").click(function () {
        getPrevRound();
    });

    function removeProperties(className) {
        $(className).css("opacity", 1);
        $(className).css("cursor", "pointer");
        $(className).removeAttr('disabled');
        $(className).removeClass('disable-button');
    }

    /**
     *
     */
    function removeTimelineDisabledFromButtons() {
        removeProperties(".timeline__prev-button");
        removeProperties(".timeline__next-button");
        removeProperties(".timeline__start-button");
        removeProperties(".timeline__stop-button");
    }

    /**
     *
     */
    function getPrevRound() {
        if (actualRound < 1) {
            actualRound = maxRound - 1;
        } else {
            actualRound--;
        }

        setMapSource(getCoordinateElement(all_coordinates, actualRound));
    }

    /**
     *
     */
    function getNextRound() {
        if (actualRound >= maxRound - 1) {
            actualRound = 0;
        } else {
            actualRound++;
        }

        setMapSource(getCoordinateElement(all_coordinates, actualRound));
    }

    /**
     *
     */
    function hideStartButton() {
        $(".timeline__start-button").hide();
        $(".timeline__stop-button").show();
    }

    /**
     *
     */
    function showStartButton() {
        $(".timeline__stop-button").hide();
        $(".timeline__start-button").show();
    }

    /**
     *
     */
    function timelineStop() {
        isPlay = false;
        showStartButton();
    }

    /**
     *
     */
    function timelineStart() {
        isPlay = true;
        hideStartButton();
        automaticMapSourceSetting(all_coordinates);
    }

    /**
     *
     */
    function disableLoader() {
        $("#loader").hide();
    }

    /**
     *
     * @param uri
     * @returns {string}
     */
    function getApiUrl(uri) {
        return BASE_API_URL + uri;
    }

    /**
     *
     */
    function showHelpText() {
        $("#help-text").addClass("fade-in");

        document.getElementById("help-text").addEventListener(
            "animationend", function () {
            $(this).addClass("animation-ended");
        });
    }

    /**
     *
     */
    function hideHelpText() {
        $("#help-text").addClass("fade-out");

        document.getElementById("help-text").addEventListener(
            "animationend", function () {
                $(this).removeClass("animation-ended");
                $(this).hide();
            }
        );
    }

    /**
     *
     * @returns {string}
     */
    function getCurrentDate() {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        today = dd + '/' + mm;

        return today;
    }

    function getCurrentTime() {
        let actualHour = new Date().toISOString().slice(11, 16)

        return actualHour;
    }

    /**
     *
     */
    function rotateWindIndicator(deg) {
        var arrow = document.getElementById("wind-info__arrow")
        arrow.style.transform = "rotate(" + deg + "deg)";
        arrow.style.transition = "transform 1s";
    }

    /**
     *
     */
    document.addEventListener(window.HackathonConfigurator.EVENT_CHANGE_CONFIGURATION, (e) => {
        rotateWindIndicator(window.HackathonConfigurator.getConfiguration('wind_deg'));
    });

    /**
     *
     */
    function startButtonPulse() {
        $("#button--apply-configuration").addClass("pulse");
    }

    /**
     *
     */
    function stopButtonPulse() {
        var button = $("#button--apply-configuration");

        if (button.hasClass("pulse")) {
            button.removeClass("pulse");
        }
    }

    /**
     *
     */
    function generateTimelineSteps() {
        var left = 0;

        for (var i = 0; i < 21; i++) {
            var node = document.createElement("div");
            node.classList.add("timeline__line-step");
            node.style.left = left + "px";

            switch (i) {
                case 0:
                case 10:
                case 20:
                    node.classList.add("timeline__line-step--primary");
                    break;
                case 5:
                case 15:
                    node.classList.add("timeline__line-step--secondary");
                    break;
            }

            document.getElementById("timeline__line").appendChild(node);
            left += 40;
        }
    }
});
