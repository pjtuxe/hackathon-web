(() => {
    /**
     *
     */
    const automatizer = () => {
        var elements = document.querySelectorAll('[data-config-disableable]');

        if (window.HackathonConfigurator.configuration.use_prediction) {
            document.querySelector("[data-prediction-text]").innerHTML = "The weather data will be auto-aligned with a machine learning model.<br>If you hover on an examined point, you can see the predictions below.";
            elements.forEach(el => {
                el.setAttribute('disabled', 'disabled');
                el.style.display = 'none';
            });
        } else {
            document.querySelector("[data-prediction-text]").innerHTML = "";
            document.querySelector("[data-prediction-text]").innerHTML = "";
            elements.forEach(el => {
                el.removeAttribute('disabled');
                el.style.display = 'block';
            });
        }

        if (window.HackathonConfigurator.configuration.confidence !== undefined) {
            var text = document.querySelector("[data-confidence-text]");
            var container = document.querySelector("[data-confidence-text-container]");
            container.style.display = "block";
            text.innerHTML = `${(window.HackathonConfigurator.configuration.confidence * 100).toFixed(2)}%`;
        }
    };

    /**
     * Listens for configuration changes, and if the prediction is set to on, disabling the inputs
     */
    document.addEventListener(window.HackathonConfigurator.EVENT_CHANGE_CONFIGURATION, automatizer);

    // Initializing the automatizer
    automatizer();
})();
