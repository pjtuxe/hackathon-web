(() => {
    /**
     * @type {string}
     */
    const EVENT_CHANGE_CONFIGURATION = 'hackathonChangeConfiguration';

    /**
     * @type {{}}
     */
    const configuration = {};

    /**
     * @param config
     * @returns {*}
     */
    const getConfiguration = config => {
        return configuration[config];
    };

    /**
     * @param {String} config
     * @param {*} value
     */
    const setConfiguration = (config, value) => {
        configuration[config] = value;
        triggerEvent(EVENT_CHANGE_CONFIGURATION, { config, value });
    };

    /**
     * @param {Object} config
     */
    const setConfigurations = config => {
        for (var i in config) {
            if (config.hasOwnProperty(i)) {
                setConfiguration(i, config[i]);
            }
        }
    };

    /**
     * @param {String} event
     * @param {*} detail
     */
    const triggerEvent = (event, detail) => {
        document.dispatchEvent(new CustomEvent(event, { detail }));
    };

    /**
     *
     */
    document.addEventListener(EVENT_CHANGE_CONFIGURATION, (e) => {
        // Changing the actual value of the HTML element
        document
            .querySelectorAll(`[data-config='${e.detail.config}']`)
            .forEach(element => {
                if ('checkbox' === element.getAttribute('type')) {
                    element.checked = !!e.detail.value;
                } else {
                    element.value = e.detail.value;
                }

                // Triggering the custom event if necessary
                var event = element.getAttribute('data-config-event');

                if (event) {
                    triggerEvent(event, e.detail);
                }
            });
        // Changing the label value of the HTML element
        document
            .querySelectorAll(`[data-config-value='${e.detail.config}']`)
            .forEach(element => {
                element.innerHTML = e.detail.value + (element.getAttribute('data-config-value-suffix') || '');
            });
    });

    /**
     *
     */
    document.querySelectorAll('[data-config]').forEach(el => {
        el.addEventListener('change', (e) => {
            if ('checkbox' === e.target.getAttribute('type')) {
                setConfiguration(e.target.getAttribute('data-config'), e.target.checked);
            } else {
                setConfiguration(e.target.getAttribute('data-config'), e.target.value);
            }
        })
    });

    // Init script
    (function () {
        setConfigurations({
            temperature: 25,
            wind_speed: 0,
            wind_deg: 0,
            humidity: 4,
            use_prediction: true,
            weather: "clear"
        });
    })();

    window.HackathonConfigurator = {
        EVENT_CHANGE_CONFIGURATION,
        configuration,
        getConfiguration,
        setConfiguration,
        setConfigurations,
        triggerEvent,
    };
})();
